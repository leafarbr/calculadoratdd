package com.br.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;

import java.awt.*;
import java.text.DecimalFormat;

public class CalculadoraTest {

    private Calculadora calculadora;

    @BeforeEach
    public void setUp()
    {
        calculadora = new Calculadora();
    }

    @Test
    public void TestaSomaDeDoisNumerosInteiro()
    {

        int resultado = calculadora.soma(1,2);
        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void TesteSomaDeDoisNumerosFlutuantes()
    {

        double resultado = calculadora.soma(2.3, 3.4);

        DecimalFormat df = new DecimalFormat("#.00");
        Assertions.assertEquals(df.format(5.70), df.format(resultado));

    }


    @Test
    public void TesteDivisaoInteiros()
    {
        int resultado = calculadora.divisao(24,3);
        Assertions.assertEquals(8, resultado);

    }

    @Test
    public void TesteDivisaoNegativos()
    {
        int resultado = calculadora.divisao(-24,3);
        Assertions.assertEquals(-8, resultado);

    }

    @Test
    public void TesteDivisaoFlutuantes()
    {
        int resultado = calculadora.divisao(24,3);
        Assertions.assertEquals(8, resultado);

    }

    @Test
    public void TestMultiplicaoInteiros()
    {
        int resultado = calculadora.multiplicacao(10,3);
        Assertions.assertEquals(30, resultado);

    }

    @Test
    public void TesteMultiplicaoNegativo()
    {
        int resultado = calculadora.multiplicacao(-5,-4);
        Assertions.assertEquals(20, resultado);

    }

    @Test
    public void TesteMultiplicaoFlutuantes()
    {
        double  resultado = calculadora.multiplicacao(2.5,4);
        Assertions.assertEquals(10, resultado);

    }

    // faça os metodos para multiplicação de numeros inteiros, numeros negativos, numeros flutuantes.
}
