package com.br.calculadora;

public class Calculadora {

    public int soma(int primeiroNumero, int segundoNumero) {
        return primeiroNumero + segundoNumero;
    }

    public double soma(double primeiroNumero, double segundoNumero) {
        return primeiroNumero + segundoNumero;
    }

    public int divisao(int primeiroNumero, int segundoNumero) {
        return primeiroNumero / segundoNumero;
    }

    public double divisao(double primeiroNumero, double segundoNumero) {
        return primeiroNumero / segundoNumero;
    }

    public int multiplicacao(int primeiroNumero, int segundoNumero)
    {
        return primeiroNumero * segundoNumero;
    }

    public double multiplicacao(double primeiroNumero, double segundoNumero)
    {
        return primeiroNumero * segundoNumero;
    }

}
